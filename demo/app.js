function addAClass(classes) {
    setTimeout(function() {
    alert("Adding class " + classes[0]);
    document.getElementById("testlist").className += " " + classes[0];
    if (classes.length > 1) {
        addAClass(classes.slice(1,classes.length));
    }
    },5000);
};

var classes = ["overthrow", "bordered", "shadowed", "rounded", "gradient",
    "backgrounded", "animated", "transitioned"];
addAClass(classes);

